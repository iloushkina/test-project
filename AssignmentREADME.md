# Assignment description and requirements

This project is a simple communication channels manager which is divided by sections. The main focus was on the `Channels` section. <br>
The user can `add`, `remove`, and `sort` channels. It is possible to add several one type channels as the user might have several emails, for instance, with different credentials. <br>

There are a few reusable components which can be used among the platform, can be found [here](src/components/common):
 - `ActionButton` 
 - `SearchBar` 
 - `SectionManager` <br>

The business specific components such as `ChannnelMananager` can be found [here](src/components/channels). <br>

The state of the selected channels is handled by saving it in the local storage managed by vuex. <br>

Components are covered with unit(Jest) and end-to-end tests (Cypress). <br>

The whole list of the requirements with statuses can be found below: 

# Must have:

| Requirements                                                                                                                                           | Status | Comments                                                        |
|--------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----------------------------------------------------------------|
| Make a new project using `Vue 3`                                                                                                                       | Done ✅ | with options API                                                |
| Make use of `Tailwind`, an utility class framework                                                                                                     | Done ✅ | I used Tailwind, <br> and in some cases custom classes with BEM |
| Make use of `Font Awesome 5` (drag indicator can be grip-vertical)                                                                                     | Done ✅ |                                                                 |
| Create a new component called `ChannelManager.vue` and (reusable) child components, <br> where you think is appropriate with the Atomic Design in mind | Done ✅ |                                                                 |
| Try to follow the design as much as possible                                                                                                           | Done ✅ | it is not 100% the same                                         |
| The order of the list can be changed by dragging the drag indicator                                                                                    | Done ✅ |                                                                 |
| Items can be removed                                                                                                                                   | Done ✅ |                                                                 |
| Items can be created (where traditionally they would be selectable by searching). <br> The icon must be a random icon from FA                          | Done ✅ |                                                                 |
| Clicking Apply will save the state                                                                                                                     | Done ✅ |                                                                 |

## Should have:

| Requirements                                                                                                        | Status         | Comments                                                                                 |
|---------------------------------------------------------------------------------------------------------------------|----------------|------------------------------------------------------------------------------------------|
| Cancel and Apply buttons appear after creating/removing/altering sort. <br> (Alterations can therefore be canceled) | Partially done | Alterations can be cancelled <br> Buttons always appear, didn't have time to finish that |
| (Some) components are unit tested via a framework as Cypress                                                        | Done ✅         |                                                                                          |

## Could have:

| Requirements                                                                            | Status | Comments   |
|-----------------------------------------------------------------------------------------|--------|------------|
| Data is stored using Pinia or Vuex (otherwise managed in data function is fine as well) | Done ✅ | using Vuex |
| List becomes scrollable with many items                                                 | Done ✅ |            |
