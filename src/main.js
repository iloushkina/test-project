import { createApp } from "vue";
import App from "./App.vue";
import channelsStore from "./store/channels/index.js";
import "./tailwind.css";

// Persist state to local storage
channelsStore.subscribe((mutation, state) => {
  localStorage.setItem("channelsStore", JSON.stringify(state));
});

// Restore state from local storage
const savedState = localStorage.getItem("channelsStore");
if (savedState) {
  channelsStore.replaceState(JSON.parse(savedState));
}

createApp(App).use(channelsStore).mount("#app");
