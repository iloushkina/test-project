import { createStore } from "vuex";

export default createStore({
  state() {
    return {
      selectedChannels: [],
    };
  },
  mutations: {
    addChannel(state, channel) {
      state.selectedChannels.push(channel);
    },
    setChannels(state, channels) {
      state.selectedChannels = channels;
    },
  },
});
