// mocked data for the channels
// for the real life project we might use axios to communicate with the backend API

import {
  faFacebook,
  faGoogle,
  faInstagram,
  faTelegram,
  faTwitter,
  faWhatsapp,
} from "@fortawesome/free-brands-svg-icons";
import {
  faComments,
  faEnvelope,
  faSms,
} from "@fortawesome/free-solid-svg-icons";

export default {
  getChannels() {
    return [
      {
        name: "WhatsApp",
        icon: faWhatsapp,
      },
      {
        name: "Facebook",
        icon: faFacebook,
      },
      {
        name: "Twitter",
        icon: faTwitter,
      },
      {
        name: "Instagram",
        icon: faInstagram,
      },
      {
        name: "Telegram",
        icon: faTelegram,
      },
      {
        name: "Google business",
        icon: faGoogle,
      },
      {
        name: "Email",
        icon: faEnvelope,
      },
      {
        name: "Website chat",
        icon: faComments,
      },
      {
        name: "SMS",
        icon: faSms,
      },
    ];
  },
};
