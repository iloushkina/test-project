# Assignment test project (frontend)
This repository contains the frontend part of the assignment test project.

## Project description
The project is a simple web application that allows users to create and manage their channels. The application is built using Vue.js (options API) with Vuex for state management, Tailwind, Cypress and Jest. For more details about the project, please refer to the [project description](AssignmentREADME.md)


## Project setup

To install the npm packages run the following command:

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
