describe("Add a new channel to the selected channel List", () => {
  it("allows the user to add and remove selected channels", () => {
    cy.visit("/");
    cy.get('[data-test="expand-button-Channels"]').click();
    cy.get('[data-test="add-channel"]').click();
    cy.get('[data-test="add-channel-modal"]').should("be.visible");
    cy.get('[data-test="add-channel-button-0"]').click();
    cy.get('[data-test="add-channel-button-1"]').click();
    cy.get('[data-test="close-modal"]').click();

    cy.get('[data-test="selected-channel-list"]').should("be.visible");
    cy.get('[data-test="selected-channel-list"]').should("have.length", 2);

    cy.get('[data-test="edit-channel"]').click();
    cy.get('[data-test="edit-channels-list-modal"]').should("be.visible");
    cy.get('[data-test="selected-channel-list"]').should("have.length", 2);
    cy.get('[data-test="remove-channel-button-0"]').click();
    cy.get('[data-test="apply-button"]').click();
    cy.get('[data-test="selected-channel-list"]').should("have.length", 1);
  });
});
