import { shallowMount } from "@vue/test-utils";
import EditChannelModal from "@/components/channels/EditChanneslModal.vue";
import ChannelItem from "@/components/channels/ChannelItem.vue";
import SearchBar from "@/components/common/SearchBar.vue";
const createWrapper = (props) => {
  return shallowMount(EditChannelModal, {
    props: {
      selectedChannels: [],
      ...props,
    },
    stubs: {
      ChannelItem,
      SearchBar,
    },
  });
};

describe("Edit channel modal component", () => {
  test("should render the component", () => {
    const wrapper = createWrapper();
    expect(wrapper.exists()).toBe(true);
  });

  test("should expose the message when there are no selected channels", () => {
    const wrapper = createWrapper();

    const emptyStateMessage = wrapper.find('[data-test="empty-state-message"]');

    expect(emptyStateMessage.exists()).toBe(true);
    expect(emptyStateMessage.text()).toBe(
      "You have not added any channels yet. Please add a channel to start receiving messages."
    );
  });

  test("should show channels when there are selected channels", () => {
    const wrapper = createWrapper({
      selectedChannels: [
        {
          id: "telegram",
          name: "telegram",
        },
        {
          id: "facebook",
          name: "Facebook",
        },
      ],
    });

    const channelItemsList = wrapper.findComponent(ChannelItem);
    expect(channelItemsList.exists()).toBe(true);
  });

  test("should show the search bar", () => {
    const wrapper = createWrapper();

    const searchBar = wrapper.findComponent(SearchBar);
    expect(searchBar.exists()).toBe(true);
  });

  test("should show the remove button", () => {
    const wrapper = createWrapper({
      selectedChannels: [
        {
          id: "telegram",
          name: "telegram",
        },
        {
          id: "facebook",
          name: "Facebook",
        },
      ],
    });

    const cancelButton = wrapper.find('[data-test="remove-channel-button-0"]');
    expect(cancelButton.exists()).toBe(true);
  });

  test("should not show the remove button when there are no selected channels", () => {
    const wrapper = createWrapper({ selectedChannels: [] });

    const cancelButton = wrapper.find('[data-test="remove-channel-button"]');
    expect(cancelButton.exists()).toBe(false);
  });

  test("should show the cancel button", () => {
    const wrapper = createWrapper();

    const saveButton = wrapper.find('[data-test="cancel-button"]');
    expect(saveButton.exists()).toBe(true);
  });

  test("should emit the cancel event when the cancel button is clicked", () => {
    const wrapper = createWrapper();

    const cancelButton = wrapper.find('[data-test="cancel-button"]');
    cancelButton.trigger("click");

    expect(wrapper.emitted("close-modal")).toBeTruthy();
  });

  test("should show the apply button", () => {
    const wrapper = createWrapper();

    const saveButton = wrapper.find('[data-test="apply-button"]');
    expect(saveButton.exists()).toBe(true);
  });

  test("should emit the apply changes event when the save button is clicked", () => {
    const wrapper = createWrapper();

    const saveButton = wrapper.find('[data-test="apply-button"]');
    saveButton.trigger("click");

    expect(wrapper.emitted("apply-changes")).toBeTruthy();
  });
});
